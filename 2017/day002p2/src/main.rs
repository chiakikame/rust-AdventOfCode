fn main() {
    let data = include_str!("../../day002p1/data/input.txt");
    let result = process_data(data);
    println!("Result is {}", result);
}

fn process_data(data: &str) -> u32 {
    data.lines().map(process_line).sum()
}

fn process_line(line: &str) -> u32 {
    let mut numbers: Vec<u32> = line.split_whitespace()
        .map(|token| token.parse::<u32>().unwrap())
        .collect();
    numbers.sort();
    calculate_line_checksum(&numbers)
}

// Assumes only one pair of candidate available
fn calculate_line_checksum(nums: &[u32]) -> u32 {
    let len = nums.len();
    for i in 0..len {
        for j in i + 1..len {
            if nums[j] % nums[i] == 0 {
                return nums[j] / nums[i];
            }
        }
    }
    panic!("Shouldn't have been here")
}

#[cfg(test)]
mod test {
    use super::*;

    fn helper(data: &str, expected: u32) {
        assert_eq!(process_line(data), expected);
    }

    #[test]
    fn test1() {
        helper("5 9 2 8", 4);
    }

    #[test]
    fn test2() {
        helper("9 4 7 3", 3);
    }

    #[test]
    fn test3() {
        helper("3 8 6 5", 2);
    }
}
