fn main() {
    let data = include_str!("../../day001/data/input.txt");
    let result = process_result(data);
    println!("result is {}", result);
}

fn process_result(s: &str) -> u32 {
    let digits: Vec<u32> = s.chars()
        .filter(|ch| ch.is_digit(10))
        .map(|ch| ch.to_digit(10).unwrap())
        .collect();
    let distance = digits.len() / 2;
    let len = digits.len();
    let mut sum = 0;

    for idx in 0..len {
        let idx_to_probe = if idx + distance >= digits.len() {
            idx + distance - len
        } else {
            idx + distance
        };
        if digits[idx] == digits[idx_to_probe] {
            sum += digits[idx];
        }
    }

    sum
}

#[cfg(test)]
mod test {
    use super::*;

    fn helper(s: &str, expected: u32) {
        assert_eq!(process_result(s), expected);
    }

    #[test]
    fn test1() {
        helper("1212", 6);
    }

    #[test]
    fn test2() {
        helper("1221", 0);
    }

    #[test]
    fn test3() {
        helper("123425", 4);
    }

    #[test]
    fn test4() {
        helper("123123", 12);
    }

    #[test]
    fn test5() {
        helper("12131415", 4);
    }
}
