use std::collections::HashSet;

fn main() {
    let raw_str = include_str!("../../data/input.day006.txt");
    let data = process_str(raw_str);
    println!("Need {} cycles to stable", process_memory_full(data));
}

fn process_str(s: &str) -> Vec<u8> {
    s.split_whitespace()
        .map(|s| s.parse::<u8>().unwrap())
        .collect()
}

fn process_memory_full(data: Vec<u8>) -> u32 {
    let mut cycles = 0;
    let mut record: HashSet<Vec<u8>> = HashSet::new();
    let mut current = data;
    let mut next;
    while !record.contains(&current) {
        next = redistribute(&current);
        record.insert(current);
        cycles += 1;
        current = next;
    }
    cycles
}

fn redistribute(src: &[u8]) -> Vec<u8> {
    let max_idx: usize = src.iter()
        .enumerate()
        .fold(MaxFolder::new(), MaxFolder::fold)
        .idx;
    let mut buf: Vec<u8> = src.to_vec();
    let mut blocks = buf[max_idx];
    buf[max_idx] = 0;
    let mut idx = max_idx + 1;
    while blocks > 0 {
        // wrap idx
        if idx == buf.len() {
            idx = 0;
        }

        // put 1 block into current position
        buf[idx] += 1;

        // next distribution cycle
        blocks -= 1;
        idx += 1;
    }
    buf
}

struct MaxFolder {
    idx: usize,
    v: u8,
}

impl MaxFolder {
    fn new() -> Self {
        MaxFolder { idx: 0, v: 0 }
    }

    fn fold(self, v: (usize, &u8)) -> Self {
        MaxFolder {
            idx: if self.v < *v.1 { v.0 } else { self.idx },
            v: if self.v < *v.1 { *v.1 } else { self.v },
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn redist() {
        let v = vec![2, 4, 1, 2];
        assert_eq!(redistribute(&v), vec![3, 1, 2, 3]);
    }

    #[test]
    fn cycle() {
        let v = vec![0, 2, 7, 0];
        assert_eq!(process_memory_full(v), 5);
    }
}
