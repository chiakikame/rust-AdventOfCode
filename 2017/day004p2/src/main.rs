use std::collections::HashSet;
use std::iter::FromIterator;

fn main() {
    let data = include_str!("../../data/input.day004.txt");
    let result = process_data(data);
    println!("Result is {}", result);
}

fn process_data(s: &str) -> usize {
    s.lines().filter(|line| is_valid_password(line)).count()
}

fn is_valid_password(s: &str) -> bool {
    let items: Vec<String> = s.split_whitespace()
        .map(|s| {
            let mut char_list: Vec<char> = s.chars().collect();
            char_list.sort();
            String::from_iter(char_list.into_iter())
        })
        .collect();
    let item_len = items.len();
    let mut set: HashSet<String> = HashSet::new();
    for i in items {
        set.insert(i);
    }
    item_len == set.len()
}

#[cfg(test)]
mod test {
    use super::is_valid_password;

    fn helper(s: &str, is_valid: bool) {
        assert!(is_valid_password(s) == is_valid);
    }

    #[test]
    fn test1() {
        helper("abcde fghij", true);
    }

    #[test]
    fn test2() {
        helper("abcde xyz ecdab", false);
    }

    #[test]
    fn test3() {
        helper("a ab abc abd abf abj", true);
    }

    #[test]
    fn test4() {
        helper("iiii oiii ooii oooi oooo", true);
    }

    #[test]
    fn test5() {
        helper("oiii ioii iioi iiio", false);
    }
}
