fn main() {
    let code = include_str!("../../data/input.day005.txt");
    let mut machine = Machine::new(code);
    println!("Need {} turns till end", machine.run_till_end());
}

struct Machine {
    instructions: Vec<i32>,
    ip: isize,
}

impl Machine {
    fn new(code: &str) -> Machine {
        let instructions: Vec<i32> = code.lines()
            .filter(|line| !line.is_empty())
            .map(|line| line.parse::<i32>().unwrap())
            .collect();
        Machine {
            instructions,
            ip: 0,
        }
    }

    fn step(&mut self) {
        if self.can_step() {
            let idx = self.ip as usize;
            let offset = self.instructions[idx] as isize;
            self.ip += offset;
            self.instructions[idx] += if offset >= 3 { -1 } else { 1 };
        }
    }

    fn can_step(&self) -> bool {
        self.ip < self.instructions.len() as isize && self.ip >= 0
    }

    fn run_till_end(&mut self) -> u32 {
        let mut counter = 0;
        while self.can_step() {
            self.step();
            counter += 1;
        }
        counter
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let mut machine = Machine::new("0\n3\n0\n1\n-3");
        assert_eq!(machine.run_till_end(), 10);
    }
}
