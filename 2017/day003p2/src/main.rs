use std::collections::HashMap;

fn main() {
    let input = 289_326;
    let v = LayerWalker::new().find(|&(_, _, v)| v > input).unwrap();
    println!("Result is {}", v.2);
}

struct LayerWalker {
    layer: u32,
    current: u32,
    pos: (i32, i32),
    current_dir: Dir,
    value_store: HashMap<(i32, i32), u32>,
}

enum Dir {
    No,
    Up,
    Left,
    Down,
    Right,
}

impl Dir {
    fn to_pos_delta(&self) -> (i32, i32) {
        match *self {
            Dir::No => (0, 0),
            Dir::Down => (0, -1),
            Dir::Left => (-1, 0),
            Dir::Right => (1, 0),
            Dir::Up => (0, 1),
        }
    }
}

impl LayerWalker {
    fn new() -> Self {
        let mut map = HashMap::new();
        map.insert((0, 0), 1);

        LayerWalker {
            layer: 1,
            current: 1,
            pos: (0, 0),
            current_dir: Dir::No,
            value_store: map,
        }
    }

    fn calculate_value_for_current_pos(&mut self) {
        let pos = self.pos;
        let v;
        {
            let map = &self.value_store;
            v = LayerWalker::getter(map, &(self.pos.0 - 1, self.pos.1 - 1)) +
                LayerWalker::getter(map, &(self.pos.0 - 1, self.pos.1)) +
                LayerWalker::getter(map, &(self.pos.0 - 1, self.pos.1 + 1)) +
                LayerWalker::getter(map, &(self.pos.0, self.pos.1 - 1)) +
                LayerWalker::getter(map, &(self.pos.0, self.pos.1 + 1)) +
                LayerWalker::getter(map, &(self.pos.0 + 1, self.pos.1 - 1)) +
                LayerWalker::getter(map, &(self.pos.0 + 1, self.pos.1)) +
                LayerWalker::getter(map, &(self.pos.0 + 1, self.pos.1 + 1));
        };
        self.value_store.insert(pos, v);
    }

    fn getter(map: &HashMap<(i32, i32), u32>, key: &(i32, i32)) -> u32 {
        map.get(key).cloned().unwrap_or_default()
    }
}

impl Iterator for LayerWalker {
    type Item = (u32, (i32, i32), u32);

    fn next(&mut self) -> Option<(u32, (i32, i32), u32)> {
        // retain current value for return
        let current = Some((self.current, self.pos, self.value_store[&self.pos]));

        // Direction adjustment
        if self.pos.0.abs() == self.pos.1.abs() {
            if self.pos.0 > 0 && self.pos.1 < 0 || self.pos == (0, 0) {
                // lower-right
                // switch layer
                self.layer += 1;
                self.current_dir = Dir::Right;
            } else if self.pos.0 > 0 && self.pos.1 > 0 {
                // upper-right
                self.current_dir = Dir::Left;
            } else if self.pos.0 < 0 && self.pos.1 > 0 {
                // upper-left
                self.current_dir = Dir::Down;
            } else if self.pos.0 < 0 && self.pos.1 < 0 {
                //lower-left
                self.current_dir = Dir::Right;
            }
        } else if self.pos.0 > 0 && self.pos.1 <= 0 && self.pos.0.abs() == self.pos.1.abs() + 1 {
            self.current_dir = Dir::Up;
        }

        // Move
        let delta = self.current_dir.to_pos_delta();
        self.pos.0 += delta.0;
        self.pos.1 += delta.1;
        self.current += 1;
        self.calculate_value_for_current_pos();

        current
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn asserter(walker: &mut LayerWalker, current: u32, pos: (i32, i32), v: u32) {
        assert_eq!(walker.next(), Some((current, pos, v)));
    }

    #[test]
    fn test() {
        let mut w = LayerWalker::new();
        asserter(&mut w, 1, (0, 0), 1);
        asserter(&mut w, 2, (1, 0), 1);
        asserter(&mut w, 3, (1, 1), 2);
        asserter(&mut w, 4, (0, 1), 4);
        asserter(&mut w, 5, (-1, 1), 5);
        asserter(&mut w, 6, (-1, 0), 10);
        asserter(&mut w, 7, (-1, -1), 11);
        asserter(&mut w, 8, (0, -1), 23);
        asserter(&mut w, 9, (1, -1), 25);
        asserter(&mut w, 10, (2, -1), 26);
        asserter(&mut w, 11, (2, 0), 54);
    }
}
