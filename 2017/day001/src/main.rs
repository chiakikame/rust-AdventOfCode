fn main() {
    let data = include_str!("../data/input.txt");
    let result = process_data(data);
    println!("Result is {}", result);
}

fn process_data(data: &str) -> u32 {
    data.chars()
        .filter(|ch| ch.is_digit(10))
        .map(|ch: char| ch.to_digit(10).unwrap())
        .fold(Folder::new(), Folder::consume)
        .conclude()
}

struct Folder {
    start: Option<u32>,
    current: u32,
    sum: u32,
}

impl Folder {
    pub fn new() -> Self {
        Folder {
            start: None,
            current: 0,
            sum: 0,
        }
    }

    pub fn consume(self, v: u32) -> Self {
        if self.start.is_none() {
            Folder {
                start: Some(v),
                current: v,
                sum: 0,
            }
        } else {
            Folder {
                start: self.start,
                current: v,
                sum: if self.current == v {
                    self.sum + v
                } else {
                    self.sum
                },
            }
        }
    }

    pub fn conclude(&self) -> u32 {
        if self.start.is_some() && self.start.unwrap() == self.current {
            self.sum + self.start.unwrap()
        } else {
            self.sum
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn test(s: &str, expected: u32) {
        assert_eq!(process_data(s), expected);
    }

    #[test]
    fn test1() {
        test("1122", 3);
    }

    #[test]
    fn test2() {
        test("1111", 4);
    }

    #[test]
    fn test3() {
        test("1234", 0);
    }

    #[test]
    fn test4() {
        test("91212129", 9);
    }

    #[test]
    fn folder() {
        let mut folder = Folder::new();
        folder = folder.consume(10);
        folder = folder.consume(20);
        assert_eq!(folder.conclude(), 0);
    }
}
