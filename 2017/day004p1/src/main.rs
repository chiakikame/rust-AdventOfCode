use std::collections::HashSet;

fn main() {
    let data = include_str!("../../data/input.day004.txt");
    let result = process_data(data);
    println!("Result is {}", result);
}

fn process_data(s: &str) -> usize {
    s.lines().filter(|line| is_valid_password(line)).count()
}

fn is_valid_password(s: &str) -> bool {
    let items: Vec<&str> = s.split_whitespace().collect();
    let mut set: HashSet<&str> = HashSet::new();
    for i in &items {
        set.insert(i);
    }
    items.len() == set.len()
}

#[cfg(test)]
mod test {
    use super::is_valid_password;

    fn helper(s: &str, is_valid: bool) {
        assert!(is_valid_password(s) == is_valid);
    }

    #[test]
    fn test1() {
        helper("aa bb cc dd ee", true);
    }

    #[test]
    fn test2() {
        helper("aa bb cc dd aa", false);
    }

    #[test]
    fn test3() {
        helper("aa bb cc dd aaa", true);
    }
}
