fn main() {
    let input = 289_326;
    println!("Result is {}", manhattan(input));
}

fn manhattan(v: u32) -> u32 {
    let mut walker = LayerWalker::new();
    let (_, pos) = walker.find(|&(current, _)| current == v).unwrap();
    pos.0.abs() as u32 + pos.1.abs() as u32
}

struct LayerWalker {
    layer: u32,
    current: u32,
    pos: (i32, i32),
    current_dir: Dir,
}

enum Dir {
    No,
    Up,
    Left,
    Down,
    Right,
}

impl Dir {
    fn to_pos_delta(&self) -> (i32, i32) {
        match *self {
            Dir::No => (0, 0),
            Dir::Down => (0, -1),
            Dir::Left => (-1, 0),
            Dir::Right => (1, 0),
            Dir::Up => (0, 1),
        }
    }
}

impl LayerWalker {
    fn new() -> Self {
        LayerWalker {
            layer: 1,
            current: 1,
            pos: (0, 0),
            current_dir: Dir::No,
        }
    }
}

impl Iterator for LayerWalker {
    type Item = (u32, (i32, i32));

    fn next(&mut self) -> Option<(u32, (i32, i32))> {
        // retain current value for return
        let current = Some((self.current, self.pos));

        // Direction adjustment
        if self.pos.0.abs() == self.pos.1.abs() {
            if self.pos.0 > 0 && self.pos.1 < 0 || self.pos == (0, 0) {
                // lower-right
                // switch layer
                self.layer += 1;
                self.current_dir = Dir::Right;
            } else if self.pos.0 > 0 && self.pos.1 > 0 {
                // upper-right
                self.current_dir = Dir::Left;
            } else if self.pos.0 < 0 && self.pos.1 > 0 {
                // upper-left
                self.current_dir = Dir::Down;
            } else if self.pos.0 < 0 && self.pos.1 < 0 {
                //lower-left
                self.current_dir = Dir::Right;
            }
        } else if self.pos.0 > 0 && self.pos.1 <= 0 && self.pos.0.abs() == self.pos.1.abs() + 1 {
            self.current_dir = Dir::Up;
        }

        // Move
        let delta = self.current_dir.to_pos_delta();
        self.pos.0 += delta.0;
        self.pos.1 += delta.1;
        self.current += 1;

        current
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn asserter(walker: &mut LayerWalker, current: u32, pos: (i32, i32)) {
        assert_eq!(walker.next(), Some((current, pos)));
    }

    #[test]
    fn test() {
        let mut w = LayerWalker::new();
        asserter(&mut w, 1, (0, 0));
        asserter(&mut w, 2, (1, 0));
        asserter(&mut w, 3, (1, 1));
        asserter(&mut w, 4, (0, 1));
        asserter(&mut w, 5, (-1, 1));
        asserter(&mut w, 6, (-1, 0));
        asserter(&mut w, 7, (-1, -1));
        asserter(&mut w, 8, (0, -1));
        asserter(&mut w, 9, (1, -1));
        asserter(&mut w, 10, (2, -1));
        asserter(&mut w, 11, (2, 0));
    }

    fn helper_manhattan(v: u32, dist: u32) {
        assert_eq!(manhattan(v), dist);
    }

    #[test]
    fn test_m1() {
        helper_manhattan(1, 0);
    }

    #[test]
    fn test_m2() {
        helper_manhattan(12, 3);
    }

    #[test]
    fn test_m3() {
        helper_manhattan(23, 2);
    }

    #[test]
    fn test_m4() {
        helper_manhattan(1024, 31);
    }
}
