fn main() {
    let data = include_str!("../data/input.txt");
    let result = process_data(data);
    println!("Result is {}", result);
}

fn process_data(data: &str) -> u32 {
    data.lines().map(process_line).sum()
}

fn process_line(line: &str) -> u32 {
    line.split_whitespace()
        .map(|token| token.parse::<u32>().unwrap())
        .fold(LineChecksumFolder::new(), LineChecksumFolder::consume)
        .conclude()
}

struct LineChecksumFolder {
    current_min: u32,
    current_max: u32,
}

impl LineChecksumFolder {
    pub fn new() -> Self {
        LineChecksumFolder {
            current_max: u32::min_value(),
            current_min: u32::max_value(),
        }
    }

    pub fn consume(self, v: u32) -> Self {
        LineChecksumFolder {
            current_max: if self.current_max < v {
                v
            } else {
                self.current_max
            },
            current_min: if self.current_min > v {
                v
            } else {
                self.current_min
            },
        }
    }

    pub fn conclude(self) -> u32 {
        self.current_max - self.current_min
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn helper(line: &str, expected: u32) {
        assert_eq!(process_line(line), expected);
    }

    #[test]
    fn test1() {
        helper("5 1 9 5", 8);
    }

    #[test]
    fn test2() {
        helper("7 5 3", 4);
    }

    #[test]
    fn test3() {
        helper("2 4 6 8", 6);
    }

    #[test]
    fn test_full() {
        let data = "5 1 9 5\n7 5 3\n2 4 6 8\n";
        assert_eq!(process_data(data), 18);
    }
}
