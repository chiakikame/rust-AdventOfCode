fn main() {
    let data = include_str!("../data/input.txt");
    let result = calculate_floor(data);
    println!("{}", result);
}

fn calculate_floor(s: &str) -> isize {
    s.chars()
        .map(|c| match c {
            '(' => 1,
            ')' => -1,
            _ => 0,
        })
        .sum::<isize>()
}

#[cfg(test)]
mod test {
    use super::calculate_floor;
    /*
    (()) and ()() both result in floor 0.
    ((( and (()(()( both result in floor 3.
    ))((((( also results in floor 3.
    ()) and ))( both result in floor -1 (the first basement level).
    */
    fn test_helper(incoming: &str, expect: isize) {
        assert_eq!(calculate_floor(incoming), expect);
    }

    #[test]
    fn t1() {
        test_helper("(())", 0);
        test_helper("()()", 0);
    }

    #[test]
    fn t2() {
        test_helper("(((", 3);
        test_helper("(()(()(", 3);
        test_helper("))(((((", 3);
    }

    #[test]
    fn t3() {
        test_helper("())", -1);
        test_helper("))(", -1);
    }
}
