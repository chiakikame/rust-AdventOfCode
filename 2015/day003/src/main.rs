use std::collections::HashSet;

fn main() {
    let data = include_str!("../data/input.txt");
    let result = process_content(data);
    println!("Result: {}", result);
}

fn process_content(content: &str) -> usize {
    let mut point = Point::new();
    let mut visited_houses: HashSet<Point> = HashSet::new();
    visited_houses.insert(point);

    for c in content.chars().filter(|c| Direction::is_dir_char(*c)) {
        point = point.move_to(Direction::from(c));
        visited_houses.insert(point);
    }

    visited_houses.len()
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    pub fn new() -> Self {
        Point { x: 0, y: 0 }
    }

    pub fn move_to(&self, direction: Direction) -> Self {
        let dx = match direction {
            Direction::Right => 1,
            Direction::Left => -1,
            _ => 0,
        };
        let dy = match direction {
            Direction::Up => 1,
            Direction::Down => -1,
            _ => 0,
        };

        Point {
            x: self.x + dx,
            y: self.y + dy,
        }
    }
}

#[derive(Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn is_dir_char(c: char) -> bool {
        c == '>' || c == '<' || c == '^' || c == 'v'
    }
}

impl From<char> for Direction {
    fn from(c: char) -> Self {
        match c {
            '>' => Direction::Right,
            '<' => Direction::Left,
            '^' => Direction::Up,
            'v' => Direction::Down,
            _ => panic!("No such symbol"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::process_content;
    use super::Point;
    use std::collections::HashSet;

    #[test]
    pub fn eq() {
        assert_eq!(Point::new(), Point::new())
    }

    #[test]
    pub fn hashset() {
        let mut hs = HashSet::new();
        for _ in 1..10 {
            hs.insert(Point::new());
        }
        assert_eq!(hs.len(), 1);
    }

    #[test]
    pub fn deliver1() {
        deliver(">", 2);
    }

    #[test]
    pub fn deliver2() {
        deliver("^>v<", 4);
    }

    #[test]
    pub fn deliver3() {
        deliver("^v^v^v^v^v", 2);
    }

    pub fn deliver(content: &str, expected: usize) {
        assert_eq!(process_content(content), expected);
    }
}
