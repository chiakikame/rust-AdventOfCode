
fn main() {
    let data = include_str!("../data/input.txt");
    let result = calculate_total_area(data);
    println!("Total area: {}", result);
}

fn calculate_total_area(data: &str) -> u32 {
    data.split('\n')
        .filter(|line| !line.is_empty())
        .map(|line| calculate_paper_size_for_line(line))
        .sum()
}

fn calculate_paper_size_for_line(line: &str) -> u32 {
    // No Error checking
    calculate_paper_size(parse_line(line))
}

fn parse_line(line: &str) -> (u32, u32, u32) {
    let mut parsed_numbers = line.split('x').take(3).map(
        |s: &str| s.parse::<u32>().unwrap(),
    );
    (
        parsed_numbers.next().unwrap(),
        parsed_numbers.next().unwrap(),
        parsed_numbers.next().unwrap(),
    )
}

fn calculate_paper_size(dimension: (u32, u32, u32)) -> u32 {
    let (a, b, c) = dimension;
    let mut sorted = [a, b, c];
    sorted.sort();
    2 * (a * b + b * c + c * a) + sorted[0] * sorted[1]
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn simple() {
        assert_eq!(calculate_paper_size((2, 3, 4)), 58);
        assert_eq!(calculate_paper_size((1, 1, 10)), 43);
    }

    #[test]
    fn parse() {
        assert_eq!(parse_line("2x3x4"), (2, 3, 4));
        assert_eq!(parse_line("1x1x10"), (1, 1, 10));
    }

    #[test]
    fn single_paper() {
        assert_eq!(calculate_paper_size_for_line("2x3x4"), 58);
        assert_eq!(calculate_paper_size_for_line("1x1x10"), 43);
    }

    #[test]
    fn field_test() {
        let data = "2x3x4\n1x1x10";
        assert_eq!(calculate_total_area(data), 58 + 43);
    }
}
